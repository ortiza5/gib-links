// The extension’s “action” (i.e. “what happens when clicking the button”)
chrome.action.onClicked.addListener((tab) => {

    // Inject the dialog toggle functionality into the DOM
    chrome.scripting.executeScript({
        target: { tabId: tab.id },
        function: toggle_dialog
    })

    // Run the dialog handling script in the DOM
    chrome.scripting.executeScript({
        target: { tabId: tab.id },
        files: ['gib_links/gib_links.js']
    })

    // Dump an additional CSS resource into the DOM
    chrome.scripting.insertCSS({
        target: { tabId: tab.id },
        files: ['gib_links/additional_styles.css']
    })
})


// Dialog toggler
//
// This function checks if there already is a dialog element and if not,
// creates one. If there is an open dialog element in the DOM the dialog’s
// close function is triggred. If the dialog is not open (i.e. was just
// created) then the showModal function is triggered, opening the dialog.
function toggle_dialog() {
    const body = document.getElementsByTagName('body')[0]

    if (document.getElementById('gib-links-extension') === null) {
        const generate_dialog = document.createElement('dialog')
        generate_dialog.id = 'gib-links-extension'
        body.appendChild(generate_dialog)
    }

    const dialog = document.getElementById('gib-links-extension')

    if (dialog.open) {
        dialog.close()
    } else {
        dialog.showModal()
    }
}
