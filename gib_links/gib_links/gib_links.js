// Shorthand function name for translations
//
// Instead of `chrome.i18n.getMessage` this function can be used to get a
// translated message string. Only one optional placeholder is supported.
//
// Usage: _('messageName', 'my cool placeholder string')
//
// @return string The return value of chrome.i18n.getMessage
function _(message, placeholder) {
    return chrome.i18n.getMessage(message, placeholder)
}


// Get the dialog element
//
// Using `var` here for to prevent unnecessary annoying detection if the
// variable is already set and using the correct element.
var dialog = document.getElementById('gib-links-extension')


// Get and filter links to list
//
// When run, the function gets the current value of the input field and
// converts it to a regex object. All links on the page are then getting
// filtered using this regex object and the links list is then being filled
// using the filtered list of links.
function populate_list() {
    const links = document.getElementsByTagName('a')
    const list = document.getElementById('gib-links-extension-list')
    const filter = document.getElementById('gib-links-extension-filter').value
    const regex = new RegExp(filter);
    const title = document.getElementById('gib-links-extension-title')

    // (re-)set the counter
    let counter = 0

    // Clean the list
    list.innerHTML = ''

    // Populate the links list based on the given filter
    for(var i = 0; i < links.length; i++) {
        const href = links[i].href
        if (regex.test(href) === true && href.length > 0) {
            const title = links[i].innerText
            const li = document.createElement('li')
            counter += 1
            li.title = links[i].innerText.trim().replace(/\s+/g, ' ')
            li.appendChild(document.createTextNode(href))
            list.appendChild(li)
        }
    }

    // Write title + counter value
    title.innerHTML = _('listTitle', counter.toString())
}


// Prepare the modal dialog
//
// This function defines all the elements of the modal dialog and adds the
// prepared dialog to the DOM.
function prepare_dialog() {
    const filter = document.createElement('input')
    const list = document.createElement('ol')
    const close = document.createElement('button')
    const title = document.createElement('h1')

    // Title
    title.id = 'gib-links-extension-title'
    title.innerHTML = chrome.i18n.getMessage('short')

    // List
    list.id = 'gib-links-extension-list'
    list.innerHTML = '<li class="spinner"></li>'

    // Input field
    filter.id = 'gib-links-extension-filter'
    filter.placeholder = _('filterPlaceholder')

    // Close button
    close.appendChild(document.createTextNode(_('closeDialog')))
    close.addEventListener('click', function() { dialog.close(); })

    // Event listener for the input field. If the user presses enter, the
    // event fires and (re-)populates the links list.
    //
    // @see populate_list()
    filter.addEventListener('keypress', function (e) {
        if (e.key === 'Enter') {
            list.innerHTML = '<li class="spinner"></li>'
            requestIdleCallback(populate_list)
        }
    })

    // Add the prepared elements to the dialog DOM node
    dialog.appendChild(title)
    dialog.appendChild(close)
    dialog.appendChild(filter)
    dialog.appendChild(list)
}


// When the dialog is open clean it, prepare it, populate the list and add
// the on-close event listener to remove the object from DOM when closing the
// dialog to clean up when not needed.
if (dialog.open) {
    dialog.innerHTML = ''
    prepare_dialog()
    requestIdleCallback(populate_list)

    // Event listener for removind the dialog DOM node when dialog is closed
    // by either clicking the extension’s button again, hitting escape, or by
    // clicking the lose button (or any other action that triggers the “close”
    // event of the dialog DOM node)
    dialog.addEventListener('close', function () {
        dialog.parentNode.removeChild(dialog)
    })
}
