**_Gib Links!_ - A Chrome extensions that gets all links from a page and shows them in an interactively filterable list**

WIP

## Local testing

To locally test the extension just open `test_links.html` in the browser. By default 100 links are generated. To change this, just append `#X` to the URL where `X` is the number of links you want to generate for testing.

## Licensing

All code and configuration is licensed under the MIT license. The icon was taken from the *Tango Desktop Project*. The icons of this project are under public domain.
